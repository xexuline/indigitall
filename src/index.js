const express = require('express');
const bodyParser = require('body-parser');
const api = require('./routes/api');

const app = express();

app.use((req, res, next) => {
  res.header('Access-Control-Allow-Origin', '*');
  res.header(
    'Access-Control-Allow-Headers',
    'Authorization, X-API-KEY, Origin, X-Requested-With, Content-Type, Accept, Access-Allow-Request-Method',
  );
  res.header('Access-Control-Allow-Methods', 'GET,POST,PUT,DELETE');
  res.header('Allow', 'GET,POST,PUT,DELETE');
  next();
});

app.use('/api', api);

module.exports = app;
