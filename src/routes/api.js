const express = require('express');
const userRoutes = require('./user/user');

const api = express.Router();

api.use(userRoutes);

module.exports = api;
