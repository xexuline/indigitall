const express = require('express');
const user = require('../../middleware/userMd');

const userRoutes = express.Router();
const urlBase = '/user';

userRoutes.get(`${urlBase}`, user.getAllUsers);
userRoutes.post(`${urlBase}/:id`, (req, res) => {
  let id = req.params.id;
  res.status(200).send({ id });
});
userRoutes.put(`${urlBase}/:id`, (req, res) => {
  let updated = req.body;
  console.log(req.body);
  res.status(200).send({ updated });
});
userRoutes.delete(`${urlBase}/:id`, (req, res) => {
  res.status(200).send({ deleted: req.params.id });
});

module.exports = userRoutes;
